//
//  NewsApiService.swift
//  MVVM_NK
//
//  Created by Admin on 2/22/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class NewsApiService {
    func getNews(completion: @escaping ([News], Error?) -> Void) {
        var newsArray: [News] = []
        
        let url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&pageSize=99&apiKey=\(API.News.key)")
        URLSession.shared.dataTask(with: url!) { (data, responce, error) in
            do {
                let dictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                guard let newsDictionaries = dictionary.object(forKey: "articles") as? [NSDictionary] else {
                    return
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                
                for news in newsDictionaries {
                    if let title = news.object(forKey: "title") as? String,
                        let description = news.object(forKey: "description") as? String,
                        let publishedAt = news.object(forKey: "publishedAt") as? String,
                        let imageUrl = news.object(forKey: "urlToImage") as? String,
                        let date = dateFormatter.date(from: publishedAt) {
                        newsArray.append(News(title: title, description: description, date: date, urlToImage: URL(string: imageUrl)))
                    }
                }
                
                completion(newsArray, nil)
            } catch let error {
                print(error)
            }
        }.resume()
    }
}
