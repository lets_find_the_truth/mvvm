//
//  ImageApiService.swift
//  MVVM_NK
//
//  Created by Admin on 2/22/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class ImageApiService {
    func getImageData(url: URL, completion: @escaping (Data) -> Void) {
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            completion(data)
        }).resume()
    }
}
