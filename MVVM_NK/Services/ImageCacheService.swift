//
//  ImageCacheService.swift
//  MVVM_NK
//
//  Created by Admin on 2/26/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class ImageCacheService {
    static var shared = ImageCacheService()
    private init() {}
    var imageCache = NSCache<NSString, NSData>()
}
