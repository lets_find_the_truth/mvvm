//
//  HomeDetailViewModel.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class HomeDetailViewModel {
    private var router: HomeDetailRouter!
    var news: News! {
        didSet {
            
        }
    }
    var imageData: Box<Data?> = Box(nil)
    
    init(router: HomeDetailRouter) {
        self.router = router
    }
    
    func viewLoad() {
        let imageCache = ImageCacheService.shared.imageCache
        if let imageURL = news.urlToImage {
            if let imageData = imageCache.object(forKey: NSString(string: imageURL.absoluteString)) {
                self.imageData.value = imageData as Data
            } else {
                ImageApiService().getImageData(url: imageURL) { (data) in
                    imageCache.setObject(NSData(data: data), forKey: NSString(string: imageURL.absoluteString))
                    self.imageData.value = data
                }
            }
            
        }
    }
}
