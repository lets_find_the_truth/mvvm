//
//  HomeDetailViewController.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class HomeDetailViewController: UIViewController {
    var viewModel: HomeDetailViewModel!

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = viewModel.news.title
        descriptionLabel.text = viewModel.news.description
        viewModel.imageData.bind { [unowned self] in
            if let data = $0 {
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data)
                }
            }
        }
        viewModel.viewLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
