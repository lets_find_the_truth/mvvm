//
//  SignUpRouter.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SignUpRouter {
    weak var viewController: SignUpViewController?
    
    func showHomeScreen() {
        let homeViewController = UIStoryboard.init(name: "Home", bundle: .main).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let homeRouter = HomeRouter(viewController: homeViewController)
        let homeViewModel = HomeViewModel(router: homeRouter)
        homeViewController.viewModel = homeViewModel
        viewController?.navigationController?.pushViewController(homeViewController, animated: true)
    }
    
    init (viewController: SignUpViewController) {
        self.viewController = viewController
    }
    
    func showAlertWith(title: String, text: String) {
        let alertViewController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        viewController?.present(alertViewController, animated: true, completion: {
            print("complete")
        })
    }
}
