//
//  SignUpViewModel.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Firebase

class SignUpViewModel {
    private var router: SignUpRouter!
    
    init(router: SignUpRouter) {
        self.router = router
    }
    
    func signUpWith(email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard error == nil else {
                self.router.showAlertWith(title: "Sign up error", text: error!.localizedDescription)
                return
            }
            self.router.showHomeScreen()
        }
    
    }
}
