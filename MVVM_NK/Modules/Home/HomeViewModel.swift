//
//  HomeViewModel.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Firebase

class HomeViewModel {
    private var router: HomeRouter!
    private var newsApiService = NewsApiService()
    private var newsDetailViewModel: HomeDetailViewModel
    var userName: Box<String> = Box("")
    private var newsCellModels: [HomeNewsTableViewCellModel] = []
    var filterdNewsCellModels: [HomeNewsTableViewCellModel] = []
    
    init (router: HomeRouter) {
        self.router = router
        newsDetailViewModel = HomeDetailViewModel(router: router.homeDetailRouter)
        if let user = Auth.auth().currentUser, let email = user.email {
            userName.value = email
        }
    }
    
    func getNews(completion: @escaping ()-> Void) {
        newsApiService.getNews { (news, error) in
            for item in news {
                self.newsCellModels.append(HomeNewsTableViewCellModel(news: item))
            }
            self.filterdNewsCellModels = self.newsCellModels
            completion()
        }
    }
    
    func searchNews(searchText: String, completion: () -> Void) {
        guard !searchText.isEmpty else {
            filterdNewsCellModels = newsCellModels
            completion()
            return
        }
        filterdNewsCellModels = newsCellModels.filter({ (news) -> Bool in
            news.title.lowercased().contains(searchText.lowercased())
        })
        completion()
    }
    
    func showNewsDetailScreenForNews(number: Int) {
        guard number < filterdNewsCellModels.count else { return }
        newsDetailViewModel.news = filterdNewsCellModels[number].news
        router.showNewsDetailScreen(newsDetailViewModel: newsDetailViewModel)
    }
    
    func logOut() {
        do {
            try Auth.auth().signOut()
            router.logOut()
        } catch let error{
            router.showAlertWith(title: "Log out error", text: error.localizedDescription)
        }
    }
}
