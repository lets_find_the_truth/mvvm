//
//  HomeNewsTableViewCell.swift
//  MVVM_NK
//
//  Created by Admin on 2/22/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class HomeNewsTableViewCell: UITableViewCell {
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var viewModel: HomeNewsTableViewCellModel! {
        didSet {
            configureCell()
        }
    }
    
    private func configureCell() {
        if let imageData = viewModel.imageData.value {
            newsImageView.image = UIImage(data: imageData)
        } else {
            viewModel.imageData.bind {
                if let data = $0 {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.newsImageView.image = image
                    }
                }
            }
        }
        titleLabel.text = viewModel.title
        dateLabel.text = viewModel.dateString
    }
}
