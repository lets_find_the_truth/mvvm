//
//  HomeNewsTableViewCellModel.swift
//  MVVM_NK
//
//  Created by Admin on 2/22/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class HomeNewsTableViewCellModel {
    var imageData: Box<Data?>
    var dateString: String
    var title: String
    var news: News
    private var imageApiService = ImageApiService()
    
    init(news: News) {
        self.news = news
        self.imageData = Box(nil)
        self.title = news.title
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let imageCache = ImageCacheService.shared.imageCache
        self.dateString = formatter.string(from: news.date)
        if let imageURL = news.urlToImage {
            if let imageData = imageCache.object(forKey: NSString(string: imageURL.absoluteString)) {
                self.imageData.value = imageData as Data
            } else {
                imageApiService.getImageData(url: imageURL) { (data) in
                    imageCache.setObject(NSData(data: data), forKey: NSString(string: imageURL.absoluteString))
                    self.imageData.value = data
                }
            }
          
        }
    }
}
