//
//  HomeViewController.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var greetingLabel: UILabel!
    var viewModel: HomeViewModel! {
        didSet {
            viewModel.getNews {
                DispatchQueue.main.async {
                    self.newsTableView.reloadData()
                }
            }
        }   
    }
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsTableView.delegate = self
        newsTableView.dataSource = self
        viewModel.userName.bind { [unowned self] in
            self.greetingLabel.text = "Hello, \($0)"
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func logOutButtonDidTap(_ sender: UIButton) {
        viewModel.logOut()
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filterdNewsCellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeNewsCell", for: indexPath) as! HomeNewsTableViewCell
        print("Cell", indexPath.row)
        print(Date())
        cell.viewModel = viewModel.filterdNewsCellModels[indexPath.row]
        print("return", indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.showNewsDetailScreenForNews(number: indexPath.row)
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.searchNews(searchText: searchText) {
            newsTableView.reloadData()
        }
    }
}


