//
//  HomeRouter.swift
//  MVVM_NK
//
//  Created by Admin on 2/21/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class HomeRouter {
    weak var viewController: HomeViewController?
    var homeDetailRouter: HomeDetailRouter!
    
    func showSignUpScreen() {
        let signUpViewController = UIStoryboard.init(name: "Login", bundle: .main).instantiateViewController(withIdentifier: "SignUpViewController")
        viewController?.navigationController?.pushViewController(signUpViewController, animated: true)
    }
    
    init (viewController: HomeViewController) {
        self.homeDetailRouter = HomeDetailRouter(viewController: viewController)
        self.viewController = viewController
    }
    
    func showNewsDetailScreen(newsDetailViewModel: HomeDetailViewModel) {
        let newsDetailViewController = UIStoryboard.init(name: "Home", bundle: .main).instantiateViewController(withIdentifier: "HomeDetailViewController") as! HomeDetailViewController
        newsDetailViewController.viewModel = newsDetailViewModel
        viewController?.navigationController?.pushViewController(newsDetailViewController, animated: true)
    }
    
    func logOut() {
        let loginViewController = UIStoryboard.init(name: "Login", bundle: .main).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let loginRouter = LoginRouter(viewController: loginViewController)
        let loginViewModel = LoginViewModel(router: loginRouter)
        loginViewController.viewModel = loginViewModel
        viewController?.present(loginViewController, animated: true, completion: nil)
    }
    
    func showAlertWith(title: String, text: String) {
        let alertViewController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        viewController?.present(alertViewController, animated: true, completion: {
            print("complete")
        })
    }
}
