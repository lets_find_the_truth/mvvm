//
//  LoginViewController.swift
//  MVVM_NK
//
//  Created by Admin on 2/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var greetingImageView: UIImageView!
    @IBOutlet weak var signInButton: UIButton! {
        didSet {
         
        }
    }
    var viewModel: LoginViewModel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signUpButtonDidTap(_ sender: UIButton) {
        viewModel.signUp()
    }
    
    @IBAction func signInButtonDidTap(_ sender: UIButton) {
        viewModel.signInWith(email: emailTextField.text!, password: passwordTextField.text!)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
