//
//  LoginViewModel.swift
//  MVVM_NK
//
//  Created by Admin on 2/15/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Firebase

class LoginViewModel {
    private var router: LoginRouter!
    
    func signInWith(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            guard error == nil else {
                self.router.showAlertWith(title: "Sign up error", text: error!.localizedDescription)
                return
            }
            self.router.showHomeScreen()
        }
    }
    
    func signUp() {
        router.showSignUpScreen()
    }
    
    init (router: LoginRouter) {
        self.router = router
    }
}
