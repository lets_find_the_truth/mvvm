//
//  Box.swift
//  MVVM_NK
//
//  Created by Admin on 2/20/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
}
