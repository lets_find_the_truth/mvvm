//
//  News.swift
//  MVVM_NK
//
//  Created by Admin on 2/26/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import Foundation

struct News {
    var title: String
    var description: String
    var date: Date
    var urlToImage: URL?
}
